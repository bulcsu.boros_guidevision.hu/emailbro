# Example 01 - quick email (plain-text)

from email_client import EmailBro

# Set these up yourself to test
USERNAME = 'USERNAMEGOESHERE'
PASSWORD = 'PASSWORDGOESHERER'
YOUR_EMAIL_ADDRESS = 'YOUREMAILADDRESSGOESHERE'
RECIPIENT_EMAIL_ADDRESS = 'TARGETMAILADDRESSGOESHERE'

email_message = {
    'username': USERNAME,
    'password': PASSWORD,
    'sender_address': YOUR_EMAIL_ADDRESS,
    'recipient_address': RECIPIENT_EMAIL_ADDRESS,
    'subject': 'Test email',
    'message': 'This is a test email (plain-text)',
}


email = EmailBro('smtp.gmail.com', 465)
email.quick_email(email_message)