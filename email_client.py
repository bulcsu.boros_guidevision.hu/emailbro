from smtplib import SMTP_SSL
from ssl import create_default_context


class EmailBro():
    """
    Simple SMTP email sender for plain text and HTML emails. Uses SSL
    """

    def __init__(self, smtp_server_address:str, port:int):
        """
        Initiation of the object, just sets up some parameters

        Args:
            smtp_server_address (str): The address of the smtp server. For example: 'smtp.gmail.com'
            port (int): The port 
        """
        self.server_address = smtp_server_address
        self.port = port
        self.logging = False
        self.server = None
        self.username = None


    def log(self, message:bool):
        """
        If self.logging is True prints log message

        Args:
            message (bool): The message that will be logged
        Returns:
            Nothing
        """
        if (self.logging):
            print(f'EmailBro: {message}')


    def set_logging(self, allow:bool):
        """
        Turning class logging on or off

        Args:
            allow (bool): Turn logging on or off
        Returns:
            Nothing
        """
        self.logging = allow


    def set_smtp_loggin(self, allow:bool):
        """
        Turning class smtp logging on or off

        Args:
            allow (bool): Turn logging on or off
        Returns:
            Nothing
        """
        if self.server:
            self.server.set_debuglevel(allow)

    def login(self, username:str, password:str):
        """
            Tries to connect to smtp server with given credetials. If success sets self.server for the connection.
            If fails prints error

        Args:
            username (str): email clien username
            password (str): email client password
        Returns:
            Nothing
        """
        try:
            context = create_default_context()
            self.server = SMTP_SSL(self.server_address, self.port, context=context)
            self.server.login(username, password)
            self.username = username
            self.log('EmailBro: login success')
        except Exception as e:
            self.log('EmailBro: login failed')
            print(e)


    def send_email(self, sender_address:str, reciever_address:str, subject:str, message:str, html:bool):
        """
        Sends the email based on args. If fails prints error message.

        Args:
            sender_address (str): The senders email address
            reciever_address (str): The recipient email addresses, comma separated
            subject (str): The subject of the email
            message (str): The body of the email
            html (bool): Should the email be sent as html or plain-text
        """
        try:
            if self.server:
                formated_message = f"From: {sender_address}\r\n"
                formated_message += f"To: {reciever_address}\r\n"
                if html:
                     formated_message += "Content-type: text/html\r\n"
                formated_message += f"Subject: {subject}"
                formated_message += "\r\n\r\n"
                formated_message += message
                self.server.sendmail(sender_address, reciever_address, formated_message)
                self.log('Email sent!')
            else:
                print('Not connected to server. Use login()!')
        except Exception as e:
            print(e)
    

    def close(self):
        """
        Closes the SMTP connection
        """
        if self.server:
            self.server.quit()
            self.log('Connection closed')
        else:
            print('Not connected to server. Use login()!')


    def quick_email(self, details:dict, **kwargs):
        """
        Logs in to smtp server
        Sends the email based on details(dict)
        Kwargs for extra options

        Args:
            details (dict): The details of the email to be sent. Follow this structure
                username : login username
                password: login password
                sender_address : email address of sender
                recipient_address : email address of target
                subject: emails subject
                message: email message

        Kwargs:
            debug (bool): Turns class logging on  or off
            smtp_debug (bool): Turns SMTP logging on or off
            html (bool): Should the email be sent as html or plain-text
            close (bool): Should the connection to the smtp server be closed or not
        """
        if 'debug' in kwargs:
            self.set_logging(kwargs['debug'])
            
        if 'smtp_debug' in kwargs:
            self.set_smtp_loggin(kwargs['smtp_debug'])
        
        html = False
        if 'html' in kwargs:
            html=kwargs['html']

        self.login(details['username'], details['password'])
        self.send_email(details['sender_address'], details['recipient_address'], details['subject'], details['message'], html)
        
        if 'close' in kwargs:
            if kwargs['close']:
                self.close()